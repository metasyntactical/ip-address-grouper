<?php

declare(strict_types=1);

namespace MetaSyntactical\IpGrouper;

use MetaSyntactical\IpGrouper\Exception\NoConfiguredRanges;
use MetaSyntactical\IpGrouper\Exception\NoRangeMatched;

interface DetectsIpGroups
{
    /**
     * Returns label of first matching ip range.
     *
     * @throws NoConfiguredRanges if no ip ranges have been configured
     * @throws NoRangeMatched if given ip address cannot be matched on any configured network
     */
    public function resolveIpToFirstMatchingIpRange(string $ipAddress): string;

    /**
     * Returns labels of all matching ranges.
     *
     * @return string[]
     * @throws NoConfiguredRanges if no networks have been configured
     */
    public function resolveIpToMatchingIpRangeList(string $ipAddress): array;
}
