<?php

declare(strict_types=1);

namespace MetaSyntactical\IpGrouper;

use IPTools\IP;
use IPTools\Range;
use MetaSyntactical\IpGrouper\Exception\InvalidIpRangeSpecification;
use MetaSyntactical\IpGrouper\Exception\NoConfiguredRanges;
use MetaSyntactical\IpGrouper\Exception\NoRangeMatched;

final class Detector implements ConfiguresDetector, DetectsIpGroups
{
    private $rangeMap = [];

    private $labelMap = [];

    public static function fromNetworkListAsString(string $rangeList, string $delimiter = ','): ConfiguresDetector
    {
        $object = new self();

        $rangeListExploded = explode($delimiter, $rangeList);
        if ($rangeListExploded === false) {
            $rangeListExploded = [];
        }
        foreach ($rangeListExploded as $range) {
            try {
                $object = $object->withRange('network', Range::parse($range));
            } catch (\Exception $exception) {
                throw new InvalidIpRangeSpecification($range, $exception->getMessage());
            }
        }

        return $object;
    }

    public function hasLabel(string $label): bool
    {
        return array_key_exists($label, $this->labelMap)
            && is_array($this->labelMap[$label])
            && count($this->labelMap[$label]);
    }

    public function hasRange(Range $range): bool
    {
        $rangeKey = "{$range->getFirstIP()}-{$range->getLastIP()}";

        return array_key_exists($rangeKey, $this->rangeMap)
            && is_array($this->rangeMap[$rangeKey])
            && count($this->rangeMap[$rangeKey]);
    }

    public function withRange(string $label, Range $range): Detector
    {
        $object = clone $this;

        $object->labelMap[$label] = array_merge(
            $object->labelMap[$label] ?? [],
            [$range]
        );
        $rangeKey = "{$range->getFirstIP()}-{$range->getLastIP()}";
        $object->rangeMap[$rangeKey] = array_merge(
            $object->rangeMap[$rangeKey] ?? [],
            [$range]
        );

        return $object;
    }

    public function withRangeList(string $label, Range $range, Range ...$ranges): self
    {
        $object = clone $this;

        $object = $object->withRange($label, $range);
        foreach ($ranges as $rangeItem) {
            $object = $object->withRange($label, $rangeItem);
        }

        return $object;
    }

    public function resolveIpToFirstMatchingIpRange(string $ipAddress): string
    {
        $found = $this->resolveIpToMatchingIpRangeList($ipAddress);

        if (!$found) {
            throw new NoRangeMatched($ipAddress);
        }

        return $found[0];
    }

    public function resolveIpToMatchingIpRangeList(string $ipAddress): array
    {
        if (!$this->labelMap) {
            throw new NoConfiguredRanges();
        }

        $ip = IP::parse($ipAddress);

        $found = array_filter(
            $this->labelMap,
            function (array $value) use ($ip) {
                foreach ($value as $range) {
                    /** @var Range $range */
                    if ($range->contains($ip)) {
                        return true;
                    }
                }

                return false;
            }
        );

        return array_keys($found);
    }
}
