<?php

declare(strict_types=1);

namespace MetaSyntactical\IpGrouper;

use IPTools\Network;
use IPTools\Range;
use MetaSyntactical\IpGrouper\Exception\InvalidDelimiter;
use MetaSyntactical\IpGrouper\Exception\InvalidIpRangeSpecification;

interface ConfiguresDetector
{
    /**
     * Constructs new object from network list given as delimited string.
     *
     * @throws InvalidDelimiter if no comma or semi-colon are used as delimiter for networks
     * @throws InvalidIpRangeSpecification if one of the ranges in the list is invalid
     */
    public static function fromNetworkListAsString(string $networkList, string $delimiter = ','): self;

    public function hasLabel(string $label): bool;

    public function hasRange(Range $range): bool;

    /**
     * @return ConfiguresDetector
     */
    public function withRange(string $label, Range $range);

    /**
     * @return ConfiguresDetector
     */
    public function withRangeList(string $label, Range $range, Range ...$ranges);
}
