<?php

declare(strict_types=1);

namespace MetaSyntactical\IpGrouper\Exception;

use InvalidArgumentException;

final class InvalidDelimiter extends InvalidArgumentException implements Exception
{
    public function __construct(string $invalidDelimiter, string ...$validDelimiters)
    {
        parent::__construct(
            sprintf(
                'The string "%s" is no valid delimiter. One of the following must be used: "%s"',
                $invalidDelimiter,
                implode('", "', $validDelimiters)
            )
        );
    }
}
