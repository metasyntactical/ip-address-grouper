<?php

declare(strict_types=1);

namespace MetaSyntactical\IpGrouper\Exception;

use RangeException;

final class NoConfiguredRanges extends RangeException implements Exception
{
    public function __construct()
    {
        parent::__construct(
            'There are no ranges configured.'
        );
    }
}
