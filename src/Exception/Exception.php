<?php

declare(strict_types=1);

namespace MetaSyntactical\IpGrouper\Exception;

use Throwable;

interface Exception extends Throwable
{

}
