<?php

declare(strict_types=1);

namespace MetaSyntactical\IpGrouper\Exception;

use OutOfBoundsException;

final class InvalidIpRangeSpecification extends OutOfBoundsException implements Exception
{
    public function __construct(string $invalidNetwork, string $reason)
    {
        parent::__construct(
            sprintf(
                'The string "%s" is no ip range. Reason: %s.',
                $invalidNetwork,
                $reason
            )
        );
    }
}
