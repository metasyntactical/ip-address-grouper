<?php

declare(strict_types=1);

namespace MetaSyntactical\IpGrouper\Exception;

use OutOfBoundsException;

final class NoRangeMatched extends OutOfBoundsException implements Exception
{
    public function __construct(string $ipAddress)
    {
        parent::__construct(
            sprintf(
                'The IP address "%s" did not match any range.',
                $ipAddress
            )
        );
    }
}
