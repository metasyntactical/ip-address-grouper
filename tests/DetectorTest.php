<?php

declare(strict_types=1);

namespace MetaSyntactical\IpGrouper\Tests;

use IPTools\Range;
use MetaSyntactical\IpGrouper\ConfiguresDetector;
use MetaSyntactical\IpGrouper\Detector;
use MetaSyntactical\IpGrouper\DetectsIpGroups;
use MetaSyntactical\IpGrouper\Exception\Exception;
use MetaSyntactical\IpGrouper\Exception\InvalidIpRangeSpecification;
use MetaSyntactical\IpGrouper\Exception\NoConfiguredRanges;
use MetaSyntactical\IpGrouper\Exception\NoRangeMatched;
use PHPUnit\Framework\TestCase;

class DetectorTest extends TestCase
{
    /**
     * @var Detector
     */
    private $subject;

    /**
     * @dataProvider provideFromNetworkListAsString
     * @covers \MetaSyntactical\IpGrouper\Detector::fromNetworkListAsString
     */
    public function testFromNetworkListAsString(array $networks, ?Exception $expectedException = null): void
    {
        if ($expectedException) {
            $this->expectException(get_class($expectedException));
            $this->expectExceptionMessage($expectedException->getMessage());
        }

        $actual = Detector::fromNetworkListAsString(implode(';', $networks), ';');

        foreach ($networks as $network) {
            self::assertTrue($actual->hasRange(Range::parse($network)));
        }

        self::assertFalse($actual->hasRange(Range::parse('99.99.99.0 255.255.255.0')));
    }

    public function provideFromNetworkListAsString(): array
    {
        return [
            'valid' => [
                ['127.0.0.1', '172.16.0.0/16', '192.168.178.0/24', '192.168.1.0-192.168.1.25', '10.0.0.0 255.0.0.0'],
            ],
            'invalid_range' => [
                ['127.0.0.1-127.0.0.0'],
                new InvalidIpRangeSpecification('127.0.0.1-127.0.0.0', 'Last IP is less than first'),
            ]
        ];
    }

    /**
     * @dataProvider provideHasLabel
     * @covers \MetaSyntactical\IpGrouper\Detector::hasLabel
     */
    public function testHasLabel(bool $expected, string $actualRange, string $actualLabel, string $expectedLabel): void
    {
        $subject = $this->subject->withRange($actualLabel, Range::parse($actualRange));

        self::assertEquals($expected, $subject->hasLabel($expectedLabel));
    }

    public function provideHasLabel(): array
    {
        return [
            'exists' => [
                true,
                '127.0.0.1',
                'network',
                'network',
            ],
            'not_exists' => [
                false,
                '127.0.0.1',
                'network',
                'my_network',
            ],
        ];
    }

    /**
     * @dataProvider provideHasRange
     * @covers \MetaSyntactical\IpGrouper\Detector::hasRange
     */
    public function testHasRange(bool $expected, string $actualLabel, string $actualRange, string $expectedRange): void
    {
        $subject = $this->subject->withRange($actualLabel, Range::parse($actualRange));

        self::assertEquals($expected, $subject->hasRange(Range::parse($expectedRange)));
    }

    public function provideHasRange(): array
    {
        return [
            'valid' => [
                true,
                'network',
                '127.0.0.1',
                '127.0.0.1',
            ],
            'not_exists' => [
                false,
                'network',
                '127.0.0.1',
                '192.168.1.1',
            ],
        ];
    }

    /**
     * @coversNothing
     */
    public function testSubjectImplementsInterfaceConfiguresDetector(): void
    {
        self::assertInstanceOf(ConfiguresDetector::class, $this->subject);
    }

    /**
     * @coversNothing
     */
    public function testSubjectImplementsInterfaceDetectsIpGroups(): void
    {
        self::assertInstanceOf(DetectsIpGroups::class, $this->subject);
    }

    /**
     * @dataProvider provideResolveIpToMatchingNetworkList
     * @covers \MetaSyntactical\IpGrouper\Detector::resolveIpToMatchingIpRangeList
     */
    public function testResolveIpToMatchingNetworkList(array $ranges, string $testIp, array $expectedLabels, ?Exception $expectedException = null): void
    {
        if ($expectedException) {
            $this->expectException(get_class($expectedException));
            $this->expectExceptionMessage($expectedException->getMessage());
        }

        $subject = $this->subject;
        foreach ($ranges as $range => $label) {
            $subject = $subject->withRange($label, Range::parse($range));
        }

        self::assertSame($expectedLabels, $subject->resolveIpToMatchingIpRangeList($testIp));
    }

    public function provideResolveIpToMatchingNetworkList(): array
    {
        return [
            'valid_in' => [
                ['127.0.0.1' => 'localhost', '127.0.0.2' => 'localhost', '127.0.0.0/0' => 'localnet', '192.168.178.0/24' => 'homenet', '192.168.0.0/16' => 'homesupernet'],
                '127.0.0.1',
                ['localhost', 'localnet'],
            ],
            'valid_notin' => [
                ['127.0.0.1' => 'localhost', '127.0.0.2' => 'localhost', '127.0.0.0/8' => 'localnet', '192.168.178.0/24' => 'homenet', '192.168.0.0/16' => 'homesupernet'],
                '10.0.0.3',
                [],
            ],
            'missing_ranges' => [
                [],
                '127.0.0.1',
                [],
                new NoConfiguredRanges(),
            ],
        ];
    }

    /**
     * @dataProvider provideResolveIpToFirstMatchingNetwork
     * @covers \MetaSyntactical\IpGrouper\Detector::resolveIpToFirstMatchingIpRange
     */
    public function testResolveIpToFirstMatchingNetwork(array $ranges, string $testIp, string $expectedLabel, ?Exception $expectedException = null): void
    {
        if ($expectedException) {
            $this->expectException(get_class($expectedException));
            $this->expectExceptionMessage($expectedException->getMessage());
        }

        $subject = $this->subject;
        foreach ($ranges as $range => $label) {
            $subject = $subject->withRange($label, Range::parse($range));
        }

        self::assertSame($expectedLabel, $subject->resolveIpToFirstMatchingIpRange($testIp));
    }

    public function provideResolveIpToFirstMatchingNetwork(): array
    {
        return [
            'valid_in' => [
                ['127.0.0.1' => 'localhost', '127.0.0.2' => 'localhost', '127.0.0.0/0' => 'localnet', '192.168.178.0/24' => 'homenet', '192.168.0.0/16' => 'homesupernet'],
                '127.0.0.1',
                'localhost',
            ],
            'valid_notin' => [
                ['127.0.0.1' => 'localhost', '127.0.0.2' => 'localhost', '127.0.0.0/8' => 'localnet', '192.168.178.0/24' => 'homenet', '192.168.0.0/16' => 'homesupernet'],
                '10.0.0.3',
                '',
                new NoRangeMatched('10.0.0.3'),
            ],
            'missing_ranges' => [
                [],
                '127.0.0.1',
                '',
                new NoConfiguredRanges(),
            ],
        ];
    }

    /**
     * @dataProvider provideWithRange
     * @covers \MetaSyntactical\IpGrouper\Detector::withRange
     */
    public function testWithRange(string $expectedLabel, string $expectedRange): void
    {
        $actual = $this->subject->withRange($expectedLabel, Range::parse($expectedRange));

        self::assertTrue($actual->hasLabel($expectedLabel));
        self::assertTrue($actual->hasRange(Range::parse($expectedRange)));
    }

    public function provideWithRange(): array
    {
        return [
            'valid' => [
                'network',
                '127.0.0.1',
            ]
        ];
    }

    /**
     * @dataProvider provideWithRangeList
     * @covers \MetaSyntactical\IpGrouper\Detector::withRangeList
     * @param string[] $expectedRanges
     */
    public function testWithRangeList(string $expectedLabel, array $expectedRanges): void
    {
        $rangeList = array_map(
            function (string $expectedRange): Range {
                return Range::parse($expectedRange);
            },
            $expectedRanges
        );
        $actual = $this->subject->withRangeList($expectedLabel, ...$rangeList);

        self::assertTrue($actual->hasLabel($expectedLabel));
        foreach ($rangeList as $expectedRange) {
            self::assertTrue($actual->hasRange($expectedRange));
        }
    }

    public function provideWithRangeList(): array
    {
        return [
            'valid' => [
                'network',
                ['127.0.0.1', '192.168.178.0/24'],
            ],
        ];
    }

    protected function setUp(): void
    {
        $this->subject = new Detector();
    }
}
