# metasyntactical/ip-address-grouper

This library provides a simple mechanism to check in which IP ranges of a given
list of ranges an IP is.

[![Latest Stable Version](https://img.shields.io/packagist/v/metasyntactical/ip-address-grouper.svg?style=flat-square)](https://packagist.org/packages/metasyntactical/ip-address-grouper)
[![Minimum PHP Version](https://img.shields.io/badge/php-%3E%3D%207.2,%3C%207.4-8892BF.svg?style=flat-square)](https://php.net/)

## Installation

You may use [Composer](https://getcomposer.org/) to download and install this library as well as its dependencies.

```bash
$ composer req metasyntactical/ip-address-grouper
```

## License

This library is distributed "as-is" without warranty of any kind under the terms of the MIT license.
